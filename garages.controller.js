const express = require('express')
const router = express.Router()
const connection = require('./conf/db.js')
const {validateGarageData} = require('./validator.middleware.js')

router.get('/', (req, res) => {
    // Exécution d'une requête SQL pour sélectionner toutes les voitures
    connection.query('SELECT * FROM garage', (err, results) => {
      // Gestion d'erreurs éventuelles lors de l'exécution de la requête
      if (err) {
        res.status(500).send('Erreur lors de la récupération des garages')
      } else {
        // Envoi des résultats sous forme de JSON si la requête est réussie
        res.json(results);
      }
    })
  })


  //route pour afficher un garage en fonction de son id
router.get('/:id', (req, res) => {

    const id = req.params.id;

    connection.query('SELECT * FROM garage WHERE garage_id = ?', [id], (err, results) => {
      
      if (err) {
        res.status(500).send('Erreur lors de la récupération du garage');
      } else {
        
        res.json(results);
      }
    })
  })

// router.post créer un garage
router.post('/', validateGarageData, (req, res) => { // utilisation du middleware validateGarageData pour vérifier que les données recquises sont bien présentes

  const {name, email} = req.body;

  //requête SQL pour insérer une nouvelle voiture
  connection.query('INSERT INTO garage(name, email) VALUES (?, ?)', [name, email], (err, results) => {
    
    if (err) {
      res.status(500).send('Erreur lors de la création d\'une nouvelle voiture');
    } else { 
      res.json({
          ...req.body, garage_id : results.insertId
      })
    }
  })
})

// router.put mettre à jours un garage
router.put('/:id', validateGarageData, (req, res) => {// utilisation du middleware validateGarageData pour vérifier que les données recquises sont bien présentes
  const {name, email} = req.body
  const id = req.params.id


  connection.execute('SELECT * FROM garage WHERE garage_id = ?',[id], (err, results)=>{
    if(err){
      res.send({ error: err.message})
    } else if(results.length === 0){
      res.send('Aucun garage trouvé à cet identifiant.')
    }else{

      const garageName = results[0]

      connection.execute('UPDATE garage SET name = ?, email = ? WHERE garage_id = ?', [name, email, id], (err, results)=>{
        if(err){
            res.status(500).send('Erreur de la mise à jour')
        }else{
            res.send(`Garage ${garageName.name} mis à jours.`)
        }
    })
    }
  })
})

// router.delete -> supprimer un garage
  router.delete('/:id', (req, res)=>{

    const id = req.params.id

    connection.execute('SELECT * FROM garage WHERE garage_id = ?',[id], (err, results)=>{
      if(err){
        res.send({ error: err.message})
      } else if(results.length === 0){
        res.send('Aucun garage trouvé à cet identifiant.')
      }else {

        const garageName = results[0]

        connection.execute('DELETE FROM garage WHERE garage_id = ?', [id], (err, results)=>{
          if(err){
              res.status(500).send({ error: err.message})
          }else{
  
              res.send(`garage ${garageName.name} supprimé`)
          }
      })
      }
    }) 
  })



module.exports = router;