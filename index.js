const express = require('express')// Importe le module mysql2 pour interagir avec des bases de données MySQL.
const app = express()
const cors = require('cors')
const carsController = require('./cars.controller.js')
const garagesController = require('./garages.controller.js')
const { logRequest } = require('./logger.middleware')

app.use(cors({
  origin: 'http://localhost:8080'
}
))

// Configure l'application pour parser les requêtes avec des corps au format JSON.
app.use(express.json())

// Configure l'application pour parser les requêtes avec des corps encodés en URL (utile pour les formulaires web).
app.use(
  express.urlencoded({
    extended: true,
  })
)

app.use(logRequest)

const port = 3000;// on définit le port de connexion ( http://localhost:3000)

// Utilise le contrôleur pour gérer les routes 
app.use('/cars', logRequest, carsController)//logRequest à cet endroit me permettra de voir toutes les requêtes qui auront été effectuées 

// Utilise le contrôleur pour gérer les routes 
app.use('/garages', logRequest, garagesController)//logRequest à cet endroit me permettra de voir toutes les requêtes qui auront été effectuées 

// Démarre le serveur Express et écoute sur le port spécifié
app.listen(port, () => {
  console.log(`Serveur en écoute sur le port ${port}`)
})
