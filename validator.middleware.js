const validateGarageData = (req, res, next) => {
    //vérifie que les champs attendus sont bien remplis
    const {name, email} = req.body

    let re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
    //condition pour vérifier si l'email est correcte
    if(!email.match(re)){
        return res.status(400).json({ message: 'email invalide.'})
    }

    if(!name, !email){
        return res.status(400).json({ message: 'le nom et l\'email sont des champs obligatoires.'})
    }

    next()
  }
  
  module.exports = { validateGarageData }
  