const mysql = require('mysql2');// importe le module mysql2 qui permet d'utiliser createPool() et permet de faire les requête à la BDD
require('dotenv').config()


// Configuration et création d'un pool de connexions à la base de données MySQL
const connection = mysql.createPool({
    host: process.env.DB_HOST,
    user: process.env.DB_USER, //id de la personne qui se connecte à la BDD, stocké dans le fichier .env
    database: process.env.DB_NAME, //nom de la BDD, stocké dans le fichier .env
    password: process.env.DB_PASSWORD // son mdp, stocké dans le fichier .env
  });

  // Établissement de la connexion
connection.getConnection((err) => {
    if (err instanceof Error) {
      console.log('getConnection error:', err); // s'il err est de type Error on affiche le message d'erreur
      return;
    } 
  });

  module.exports = connection