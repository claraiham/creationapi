const express = require('express');
const router = express.Router();
const connection = require('./conf/db.js')

// Définition d'une route GET pour récupérer toutes les voitures de la base de données
router.get('/', (req, res) => {
    // Exécution d'une requête SQL pour sélectionner toutes les voitures
    connection.query('SELECT * FROM car', (err, results) => {
      // Gestion d'erreurs éventuelles lors de l'exécution de la requête
      if (err) {
        res.status(500).send('Erreur lors de la récupération des voitures');
      } else {
        // Envoi des résultats sous forme de JSON si la requête est réussie
        res.json(results);
      }
    });
  });

  // route get pour récupérer les voitures correspondantes à une marque
  router.get('/brand/:brand', (req, res) => {

    const brand = req.params.brand;

    // Exécution d'une requête SQL pour sélectionner toutes les voitures appartenant à une même marque
    connection.query('SELECT * FROM car WHERE brand = ?', [brand], (err, results) => {
      
      if (err) {
        res.status(500).send('Erreur lors de la récupération des voitures correspondantes à la marque');
      } else {
        
        res.json(results);
      }
    });
  });


  // route pour récupérer une voiture en focntion de son id
  router.get('/:id', (req, res) => {

    const id = req.params.id;
    // Exécution d'une requête SQL pour sélectionner la voiture correspondant à un identifiant précis
    connection.query('SELECT * FROM car WHERE car_id = ?', [id], (err, results) => {
      
      if (err) {
        res.status(500).send('Erreur lors de la récupération de la voiture');
      } else {
        
        res.json(results);
      }
    });
  });

  // route POST pour ajouter une voiture
  router.post('/', (req, res) => {

    const {brand, model} = req.body;

    //requête SQL pour insérer une nouvelle voiture
    connection.query('INSERT INTO car(brand, model) VALUES (?, ?)', [brand, model], (err, results) => {
      
      if (err) {
        res.status(500).send('Erreur lors de la création d\'une nouvelle voiture');
      } else { 
        res.json({
            ...req.body, car_id : results.insertId
// "...req.body" Toutes les valeurs du Json, on accède à une autre propriété "car_id". Results -> permet d'acccéder au dernier id inséré
        });
      }
    });
  });

  // route pour mettre à jour une voiture
  router.put('/:id', (req, res) => {
    const {brand, model} = req.body
    const id = req.params.id

    connection.execute('UPDATE car SET brand = ?, model = ?, WHERE car_id = ?', [brand, model, id], (err, results)=>{
        if(err){
            res.status(500).send('Erreur de la mise à jour')
        }else{
            res.send(`Model et marque de la voiture mises à jour`)
        }
    })

  })

  //route pour supprimer une voiture
  router.delete('/:id', (req, res)=>{

    const id = req.params.id

            connection.execute('DELETE FROM car WHERE car_id = ?', [id], (err, results)=>{
                if(err){
                    res.status(500).send('Erreur lors de la suppression')
                }else{
        
                    res.send(`Voiture supprimée`)
                }
            }) 
  })

// méthode pour afficher la marque et le modèle de la voiture supprimée bancale
//   router.delete('/car/:id', (req, res)=>{
//     const id = req.params.id
//     connection.query('SELECT brand, model FROM car WHERE car_id = ?', [id], (err, results) => {     
//         if (err) {
//           res.status(500).send('Erreur lors de la récupération de l\'identitfiant de la voiture');
//         } else {

//             const carToDelete = results[0];
            
          
//             connection.execute('DELETE FROM car WHERE car_id = ?', [id], (err, deleteResults)=>{
                
//                 if(err){
//                     res.status(500).send('Erreur lors de la suppression')
//                 }else{
        
//                     res.send(`${carToDelete.brand} ${carToDelete.model} supprimé`)
//                 }
//             })
//         }
//       });   
//   })

module.exports = router;